#ifndef JV_COMMON_H
#define JV_COMMON_H

namespace JV {
  struct Weather {
    float temp;
    float rh;
    float pressure;
    float dewpoint;
  };
}

#endif