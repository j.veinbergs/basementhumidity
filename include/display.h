#include "U8glib.h"
#include "common.h"

#ifndef JV_DISPLAY_H
#define JV_DISPLAY_H
namespace JV {
  void DisplaySensorDataPage(Weather weatherIn, Weather weatherOut, bool venting);
}
#endif