#include "display.h"
#include "U8glib.h"
#include "common.h"


namespace JV {
    //Kā uzstādīt ekrānu: https://github.com/olikraus/u8g2/wiki/u8x8setupcpp#introduction
    U8GLIB_SH1106_128X64 Display(U8G_I2C_OPT_NONE);                     // I2C / TWI 1.3

    void DisplaySensorDataPage(Weather weatherIn, Weather weatherOut, bool venting) {
        Display.firstPage();
    do {
        Display.setFont(u8g_font_8x13);
        Display.setPrintPos(40, 10);
        Display.print("T  Rh  Dp");
        Display.setPrintPos(0, 23);
        Display.printf("In :%dC %d%% %dC", (int)weatherIn.temp, (int)weatherIn.rh, (int)weatherIn.dewpoint);
        Display.setPrintPos(0, 40);
        Display.printf("Out:%dC %d%% %dC", (int)weatherOut.temp, (int)weatherOut.rh, (int)weatherOut.dewpoint);
        Display.setPrintPos(0, 57);
        if (venting) {
            Display.printf("Venting: YES");
        } else {
            Display.printf("Venting: NO");
        }
    } while( Display.nextPage() );
    }
}