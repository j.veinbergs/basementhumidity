#include "Arduino.h"
#include <Wire.h>
#include "SparkFunBME280.h"
#include "common.h"
#include <SoftwareSerial.h>
#include <SIM900Client.h>
#include <PubSubClient.h>
#include <avr/wdt.h>

#define ULONG_MAX 4294967295 
//Bootloader aizņem 4kb? Pēc specenes atmega328P būtu jābūt 0.5kb. Izmantojamā vieta max ir 28672 
//PROGRAM: [========= ]  88.9% (used 28672 bytes from 32256 bytes)

//#define DEBUG //Print additional messages to serial monitor. Takes additional space and may not be enought for Atmega328P if all features enabled.
#define DO_RELAY // Relay connected, do turn on/off venting
//#define DO_DISPLAY //Display data on a... display.
#ifdef DO_RELAY
#define RELAY1 2
#endif

//How often data is read from sensors and printed on screen
#define DATAREFRESHINTERVAL_SEC 60
//Set the frequency of updating the relay here, in seconds. The larger the value, the less likely you will have rapid on/off cycling
#define RELAYINTERVAL_SEC 600
//Set the ventilation threshold of the DEWPOINT in degrees celcius.
#define VENTTHRESHOLD_C 1.5
//Set minimimal OUT temp in C. Will not vent under this tmep.
#define VENTMINOUTTEMP_C 15

//Set how often GPRS connection will be made to publish MQTT data. Not recommended too often. 
//Data will be sent out IMMEDIATELLY AFTER venting state changes
#define MQTTINTERVAL_SEC 120
//When this goes down to 0, data will be sent. Getsreset to MQTTINTERVAL_SEC and loop decrements this value.
int currentMqttInterval = 0; 
BME280 sensorIn; //Uses default I2C address 0x76
BME280 sensorOut;

#ifdef DO_DISPLAY
#include "display.h"
#endif

SIM900Client GPRS(7, 8, 9);
PubSubClient mqttClient(GPRS);
// the mobile network login data
#define GPRS_APN "internet.lmt.lv"
#define GPRS_USER ""
#define GPRS_PASS ""
char mqttBroker[] = "ha.jve.lv";
uint16_t mqttPort = 1883;
char mqttClientId[] = "garage";
char mqttUser[] = "garage";
char mqttPass[] = "this-has-been-changed";
char mqttTopic[] = "garage/result/json";

// Watchdog configuration. Automatically restart arduino if not responding for some period
// when firing, configure it for resuming program execution not reseting. Within ISR we do reset if multiple 8 second intervals have passed (max interval for watchdog firing)
#define WATCHDOG_MAX_COUNT_UNTIL_RESET 85 //8 sec * WATCHDOG_MAX_COUNT_UNTIL_RESET = after what time Arduino will be reset
int watchdogCount = 0;
void configure_wdt(void) {
  cli();                           // disable interrupts for changing the registers

  MCUSR = 0;                       // reset status register flags
                                   // Put timer in interrupt-only mode:                                       
  WDTCSR |= 0b00011000;            // Set WDCE (5th from left) and WDE (4th from left) to enter config mode,
                                   // using bitwise OR assignment (leaves other bits unchanged).
  WDTCSR =  0b01000000 | 0b100001; // set WDIE: interrupt enabled (7th bit)
                                   // clr WDE: reset disabled (4th bit)
                                   // and set delay interval (right side of bar) to 8 seconds
  sei();                           // re-enable interrupts
  // reminder of the definitions for the time before firing
  // delay interval patterns:
  //  16 ms:     0b000000
  //  500 ms:    0b000101
  //  1 second:  0b000110
  //  2 seconds: 0b000111
  //  4 seconds: 0b100000
  //  8 seconds: 0b100001
}

void setup() {
  configure_wdt();
  Serial.begin(19200);

  Wire.begin();
  sensorIn.setI2CAddress(0x77); //The default for the SparkFun Environmental Combo board is 0x77 (jumper open).
  sensorOut.setI2CAddress(0x76);
  if(sensorIn.beginI2C() == false) { Serial.println("IN sensor failed"); }
  if(sensorOut.beginI2C() == false) { Serial.println("OUT sensor failed"); }
#ifdef DO_RELAY
  pinMode(RELAY1, OUTPUT);
  //by default - turned on/off. Clicking wil help understand that setup has completed and relay works.
  digitalWrite(RELAY1, HIGH);
  delay(250);
  digitalWrite(RELAY1, LOW);
#endif
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
}


//quick dp approximation, http://en.wikipedia.org/wiki/Dew_point
float dewpoint(float temperature, float humidity) {
  {
    float a = 17.271;
    float b = 237.7;
    if (temperature == 0.00 && humidity == 0.00) {
      return 0;
    }
    float temp = (a * temperature) / (b + temperature) + log((double)humidity / 100);
    double Td = (b * temp) / (a - temp);
    return Td;
  }
}

int publishMqttMessage(JV::Weather *weatherIn, JV::Weather *weatherOut, bool venting)
{
  //GPRS
  GPRS.begin(19200); while (!GPRS);
  int tries = 3;
  uint8_t rcAttach;
  do {
    rcAttach = GPRS.attach(GPRS_APN, GPRS_USER, GPRS_PASS);
  } while (rcAttach != 0 && (--tries > 0));
  if (rcAttach) {
    Serial.printf(F("Could not attach GPRS. Returned: %i\n"), rcAttach);
    GPRS.powerDown();
    return rcAttach;
  }
  mqttClient.setServer(mqttBroker, mqttPort);

  if (!mqttClient.connected()) {
    if (!mqttClient.connect(mqttClientId, mqttUser, mqttPass)) {
      Serial.printf(F("MQTT Failed. RC=%i\n"), mqttClient.state());
      GPRS.powerDown();
      return mqttClient.state();
    }
  }
  
  // Concatenating like this saves memory.
  // We couldn't use sprintf because arduino by default has turned off floating point operations. And %.5f prints out just: '?'
  // So we use dtostrf to convert float to char. And put it directly into jsonResponse not some intermediate buffer that would require more space.
  // jsonResponse will builds response like: {"out":{"t": 23.44,"h": 57.26,"p":100757.34,"dp": 14.50},"basement":{"t": 23.44,"h": 57.26,"p":100757.34,"dp": 14.50},"venting":0}
  char jsonResponse[130] = "{\"out\":{\"t\":";
  const char* F = "0";
  const char* T = "0";
  const char* CURLY_BRACKET_CLOSE = "}";
  dtostrf(weatherOut->temp, 6, 2, (char*)memchr(jsonResponse, 0, sizeof(jsonResponse) - 7));
  strncat(jsonResponse, ",\"h\":", 6);
  dtostrf(weatherOut->rh, 6, 2, (char*)memchr(jsonResponse, 0, sizeof(jsonResponse) - 7));
  strncat(jsonResponse, ",\"p\":", 6);
  dtostrf(weatherOut->pressure, 6, 2, (char*)memchr(jsonResponse, 0, sizeof(jsonResponse) - 7));
  strncat(jsonResponse, ",\"dp\":", 6);
  dtostrf(weatherOut->dewpoint, 6, 2, (char*)memchr(jsonResponse, 0, sizeof(jsonResponse) - 7));
  strncat(jsonResponse, "},\"basement\":{\"t\":", 19);
  dtostrf(weatherIn->temp, 6, 2, (char*)memchr(jsonResponse, 0, sizeof(jsonResponse) - 7));
  strncat(jsonResponse, ",\"h\":", 6);
  dtostrf(weatherIn->rh, 6, 2, (char*)memchr(jsonResponse, 0, sizeof(jsonResponse) - 7));
  strncat(jsonResponse, ",\"p\":", 6);
  dtostrf(weatherIn->pressure, 6, 2, (char*)memchr(jsonResponse, 0, sizeof(jsonResponse) - 7));
  strncat(jsonResponse, ",\"dp\":", 6);
  dtostrf(weatherIn->dewpoint, 6, 2, (char*)memchr(jsonResponse, 0, sizeof(jsonResponse) - 7));
  strncat(jsonResponse, "},\"venting\":", 12);
  strncat(jsonResponse, venting ? T : F, 1);
  strncat(jsonResponse, CURLY_BRACKET_CLOSE, 1);
  // char weatherOutTemp[7] = "0";
  // char weatherOutRh[7] = "0";
  // char weatherOutPressure[7] = "0";
  // char weatherOutDewpoint[7] = "0";
  // char weatherInTemp[7] = "0";
  // char weatherInRh[7] = "0";
  // char weatherInPressure[7] = "0";
  // char weatherInDewpoint[7] = "0";
  // dtostrf(weatherOut->temp, 6, 2, weatherOutTemp);
  // dtostrf(weatherOut->rh, 6, 2, weatherOutRh);
  // dtostrf(weatherOut->pressure, 6, 2, weatherOutPressure);
  // dtostrf(weatherOut->dewpoint, 6, 2, weatherOutDewpoint);
  // dtostrf(weatherOut->temp, 6, 2, weatherInTemp);
  // dtostrf(weatherOut->rh, 6, 2, weatherInRh);
  // dtostrf(weatherOut->pressure, 6, 2, weatherInPressure);
  // dtostrf(weatherOut->dewpoint, 6, 2, weatherInDewpoint);
  // sprintf(jsonResponse, "{\"out\":{\"t\":%s,\"h\":%s, \"p\":%s, \"dp\":%s},\"basement\":{\"t\":%s,\"h\":%s, \"p\":%s, \"dp\":%s},\"venting\":%u}",
  //   weatherOutTemp, weatherOutRh, weatherOutPressure, weatherOutDewpoint,
  //   weatherInTemp, weatherInRh, weatherInPressure, weatherInDewpoint,
  //   venting
  // );
  #ifdef DEBUG
  Serial.printf("Publish to topic mqttTopic: %s\n", jsonResponse);
  #endif
  mqttClient.publish(mqttTopic, jsonResponse);
  GPRS.powerDown();
  return 0;
}

#ifdef DEBUG
void printSensorData(JV::Weather weather) {
  Serial.print("Rh: ");
  Serial.print(weather.rh, 0);
  Serial.print("% Temp: ");
  Serial.print(weather.temp, 2);
  Serial.print("C P: ");
  Serial.print(weather.pressure / 100.0F, 0);
  Serial.print("hPa Rasas punkts: ");
  Serial.print(weather.dewpoint, 2);
  Serial.println('C');
}
#endif

void enterLowPowerMode(void)
{
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);   /* EDIT: could also use SLEEP_MODE_PWR_DOWN for lowest power consumption. */
  sleep_enable();
  power_all_disable();                    // turn power off to ADC, TIMER 1 and 2, Serial Interface
  noInterrupts ();                        // turn off interrupts as a precaution
  wdt_reset();                            // reset the WatchDog before beddy bies
  sleep_enable ();                        // allows the system to be commanded to sleep
  interrupts ();                          // turn on interrupts

  sleep_cpu ();                           // send the system to sleep, night night!
  
  /* The program will continue from here after the WDT timeout*/
  sleep_disable();                        // after ISR fires, return to here and disable sleep
  power_all_enable();                     // turn on power to ADC, TIMER1 and 2, Serial Interface
}


bool venting = false;
//
int relayControlSecondCount = 59;
void loop() {
  watchdogCount = 0; //reset watchdog timer.
  unsigned long start = millis();

  //Loop start - flash onboard led
  digitalWrite(LED, HIGH);
  delay(100);
  digitalWrite(LED, LOW);
  delay(100);
  digitalWrite(LED, HIGH);
  delay(100);
  digitalWrite(LED, LOW);

  //read sensors
  struct JV::Weather weatherIn;
  struct JV::Weather weatherOut;
  weatherIn.temp = sensorIn.readTempC();
  weatherIn.rh = sensorIn.readFloatHumidity();
  weatherIn.pressure = sensorIn.readFloatPressure();
  weatherIn.dewpoint = dewpoint(weatherIn.temp, weatherIn.rh);
  weatherOut.temp = sensorOut.readTempC();
  weatherOut.rh = sensorOut.readFloatHumidity();
  weatherOut.pressure = sensorOut.readFloatPressure();
  weatherOut.dewpoint = dewpoint(weatherOut.temp, weatherOut.rh);
  
  //vent?
  if(relayControlSecondCount == RELAYINTERVAL_SEC) {
    relayControlSecondCount = 1;
    if ( weatherOut.temp > VENTMINOUTTEMP_C && (weatherOut.dewpoint + VENTTHRESHOLD_C) < weatherIn.dewpoint) {
#ifdef DEBUG
        if (venting == false) { Serial.print("Vent ON"); }
#endif
        if (!venting) {
          venting=true;
          currentMqttInterval = 0; //venting state change - send out data immediatelly
        }
#ifdef DO_RELAY
        digitalWrite(RELAY1, HIGH);
#endif
    }
    else {
#ifdef DEBUG
        if (venting == true) { Serial.print("Vent OFF"); }
#endif
#ifdef DO_RELAY
        digitalWrite(RELAY1, LOW);
#endif
        if (venting) {
          venting=false;
          currentMqttInterval = 0; //venting state change - send out data immediatelly
        }
      }
  }
  relayControlSecondCount++;
  
  //Print to serial port
#ifdef DEBUG
  Serial.print("IN: ");
  printSensorData(weatherIn);
  Serial.print("OUT: ");
  printSensorData(weatherOut);
#endif

#ifdef DO_DISPLAY
  wdt_reset();
  JV::DisplaySensorDataPage(weatherIn, weatherOut, venting);
#endif

  if (currentMqttInterval <= 0) {
    publishMqttMessage(&weatherIn, &weatherOut, venting);
    currentMqttInterval = MQTTINTERVAL_SEC;
  } else {
    currentMqttInterval -= DATAREFRESHINTERVAL_SEC;
  }

  //May be negative if millis() overflow between start and end time. Happens every ~50 days.
  unsigned long end = millis();
  unsigned long delta = start < end ? end - start : (ULONG_MAX - start) + end; //takes care of OVERFLOW
  long sleep = (DATAREFRESHINTERVAL_SEC * 1000L) - delta; //sleep may be less than 0 if DATAREFRESHINTERVAL_SEC is too small. One loop may execute 30+sec (SIM module powerup and registering to GPRS network takes time)
  wdt_reset();
  
  if (sleep >= 8000) {
    for (; sleep >= 8000; sleep-=8000) {
      enterLowPowerMode();
    }
    //Delay reminder of 8-sec interval
    delay(sleep % 8000);
  } else if (sleep > 0) {
    delay(sleep);
  }
}

//Watchdog
ISR(WDT_vect) {
    if (watchdogCount++ < WATCHDOG_MAX_COUNT_UNTIL_RESET) {
      wdt_reset();
    } else {
      //Reset device when watchdog has fired multiple times and no reset was perormed.
      // then change timer to reset-only mode with short (16 ms) fuse
      MCUSR = 0;                          // reset flags
                                          // Put timer in reset-only mode:
      WDTCSR |= 0b00011000;               // Enter config mode.
      WDTCSR =  0b00001000 | 0b000000;    // clr WDIE (interrupt enable...7th from left)
                                          // set WDE (reset enable...4th from left), and set delay interval
                                          // reset system in 16 ms...
                                          // unless wdt_disable() in loop() is reached first
    }
}